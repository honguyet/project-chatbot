<?php
/**
 * Created by PhpStorm.
 * User: HoNguyet
 * Date: 12/28/2018
 * Time: 8:32 PM
 */
?>
        <!doctype html>
<html>
<head>
    <title>BotMan Widget</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/assets/css/chat.min.css">
</head>
<body>
<script id="botmanWidget" src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/chat.js'></script>
<script>
    var botmanWidget = {
        frameEndpoint: 'chat.blade.php',
        title: 'Helpdesker',
        introMessage: 'can i help you anything?',
        displayMessageTime: true,
        // chatserver: borman.

    };
</script>
<script src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/widget.js'></script>
</body>
</html>
